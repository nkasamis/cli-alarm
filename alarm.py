import argparse, time, os
from datetime import timedelta, datetime
from playsound import playsound

def create_parser():
	parser = argparse.ArgumentParser(prog='Alarm', description='Set an alarm')
	parser.add_argument('-v', '--version', action='version', version='%(prog)s v0.01')
	parser.add_argument('-t', '--time', help='The time you want the alarm to go off. Example: 3:33AM')
	parser.add_argument('-s', '--seconds', type=int, default=0, help='Number of seconds into the future')
	parser.add_argument('-m', '--minutes', type=int, default=0, help='Number of minutes into the future')
	parser.add_argument('-H', '--hours', type=int, default=0, help='Number of hours into the future')
	parser.add_argument('--message', type=str, default='Time\'s up', help='Set a message for when the alarm goes off.')

	return vars(parser.parse_args())

def convert_minutes_seconds(mins):
	return 60*mins
	

def convert_hours_seconds(hours):
	return (60**2)*hours


def calc_total_seconds(hours, minutes, seconds):

	return (convert_hours_seconds(hours) + convert_minutes_seconds(minutes) + seconds)

def set_alarm(totalSeconds, message):
	time.sleep(totalSeconds)
	
	print(message)

def main():
	args = create_parser()

	futureTime = args['time'].split(' ')
	seconds = args['seconds']
	minutes = args['minutes']
	hours = args['hours']
	message = args['message']
	totalSeconds = 0

	if futureTime == None:
		# Check for hours, minutes, seconds
		totalSeconds = calc_total_seconds(hours, minutes, seconds)

		if totalSeconds < 60:

			print('Timer will be set for {} seconds.'.format(totalSeconds))
		else:
			if totalSeconds > 3600:
				print('Timer will be set for {} hours.').format(totalSeconds*60)
			else:
				print('Timer will be set for {} minutes.').format((totalSeconds//60))

		set_alarm(totalSeconds, message)

	else:
		now = datetime.fromtimestamp(time.time())
		nowFormat = '{}:{}'.format(str(now.hour), str(now.minute))
		currentTime = datetime.strptime(nowFormat, '%H:%M')
		alarmTime = datetime.strptime(futureTime[0], '%H:%M')

		#print(currentTime)
		#print(alarmTime)

		tdelta = alarmTime - currentTime
		
		set_alarm(tdelta.seconds, message)





main()